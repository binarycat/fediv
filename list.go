package fediv

import (
	"strings"
	"log"
)

type URLList struct {
	Raw string
	cache []string
}

func (ul *URLList) SetRaw(raw string) {
	ul.Raw = raw
	ul.cache = []string{}
}

func (ul *URLList) Items() []string {
	if len(ul.cache) != 0 { return ul.cache }
	parts := strings.Split(ul.Raw, "\n")
	ul.cache = make([]string, 0, len(parts))
	for _, part := range parts {
		part = strings.TrimSpace(part)
		if strings.HasPrefix(part, "@") {
			u, err := userObject(part[1:])
			if err != nil {
				log.Printf("webfinger %s error: ", part, err)
				continue
			}
			part = u
		}
		if isAbsURL(part) {
			ul.cache = append(ul.cache, part)
		}
	}
	return ul.cache
}
