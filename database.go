package fediv

import (
	"time"
	"os"
	"sync"
	"path/filepath"
	"encoding/json"
	"log"
	"strconv"
	"regexp"
	"errors"
)

var usernameRE = regexp.MustCompile("[a-zA-Z][a-zA-Z0-9]*")

var ErrUsername = errors.New("invalid username")

type SortMethod uint8

const (
	// oldest first
	SM_OLDEST SortMethod = iota
	// newest first
	SM_NEWEST
)

// DB represents the database backend.
type DB struct {
	sync.Mutex
	dir string
	postCache map[PostID]*Post
	queryCache map[string]*QCache
	feeds []Feed
	// id that will be given to the next created post.
	nextid PostID
}

type dbMeta struct {
	NextID PostID `json:"nextid"`
}

// open database
func NewDB() (*DB, error) {
	configDir, err := os.UserConfigDir()
	if err != nil { return nil, err }
	dir := filepath.Join(configDir, "fediv")
	var meta dbMeta
	err = readJSON(filepath.Join(dir, "db.json"), &meta)
	if err != nil { return nil, err }
	return &DB{
		dir: dir,
		nextid: meta.NextID,
		postCache: make(map[PostID]*Post),
		queryCache: make(map[string]*QCache),
	}, nil
}

func (db *DB) userPath(user string) string {
	if !usernameRE.Match([]byte(user)) {
		return "/dev/null/INVALID_USERNAME"
	}
	return filepath.Join(db.dir, "user", user)
}

func (db *DB) PutUser(user *User) error {
	if !usernameRE.Match([]byte(user.Username)) {
		return ErrUsername
	}
	db.Lock()
	defer db.Unlock()
	return writeJSON(db.userPath(user.Username), user)
}

func (db *DB) GetUser(name string) *User {
	user := new(User)
	err := readJSON(db.userPath(name), user)
	if err != nil {
		log.Printf("GetUser(%s) failed: %s", name, err)
		return nil
	}
	return user
}

func (db *DB) postPath(id PostID) string {
	return filepath.Join(
		db.dir, "post", strconv.Itoa(int(id)))
}

func (db *DB) AddPost(post *Post) error {
	db.Lock()
	defer db.Unlock()
	post.ID = db.nextid
	post.Created = time.Now()
	db.nextid += 1
	// filesystem stuff
	// eventually this will be replaced with a proper database
	metaFile, err := os.Create(filepath.Join(db.dir, "db.json"))
	// if this fails, database is likely corrupted
	if err != nil { log.Fatal("db error: ", err) }
	defer metaFile.Close()
	// if this fails, it definitly will corrupt the db
	err = json.NewEncoder(metaFile).Encode(dbMeta{NextID: db.nextid})
	if err != nil { log.Fatal("database corrupted: ", err) }
	return db.putPost(post)
}

func (db *DB) GetPost(id PostID) *Post {
	post, ok := db.postCache[id]
	if ok && post != nil {
		return post.Clone()
	}
	post = new(Post)
	err := readJSON(db.postPath(id), post)
	if err != nil {
		return nil
	}
	return post
}

func (db *DB) putPost(post *Post) error {
	err := writeJSON(db.postPath(post.ID), post)
	if err != nil {
		return err
	}
	db.postCache[post.ID] = post.Clone()
	return nil
}

func (db *DB) PutPost(post *Post) error {
	db.Lock()
	err := db.putPost(post)
	db.Unlock()
	return err
}

func (db *DB) DelPost(id PostID) error {
	db.Lock()
	defer db.Unlock()
	delete(db.postCache, id)
	return os.Remove(db.postPath(id))
}

func (db *DB) GetPostBySource(u string) *PostID {
	postList, _, _ := db.GetPage(
		SourceQuery(u), SM_NEWEST, -1, 0, 1)
	if len(postList) > 0 {
		return &postList[0]
	}
	return nil
}

func (db *DB) GetConfig() Config {
	cfg := DefaultCfg
	err := readJSON(filepath.Join(db.dir, "db.json"), &cfg)
	if err != nil { return DefaultCfg }
	return cfg
}

// the "version" value stops the list of posts from changing while they
// are being paged through.
// -1 means use the newest version
// returns list of posts, new version, and total number of posts in given version
func (db *DB) GetPage(
	q Query, sm SortMethod,
	version, offset, count int,
) ([]PostID, int, int) {
	db.Lock()
	qstr := q.String()
	qc, ok := db.queryCache[qstr]
	if !ok {
		qc = &QCache{ posts: []PostID{} }
		db.queryCache[qstr] = qc
		log.Printf("qcache entry created: %q %#v", qstr, q)
	}
	if version == -1 || version > len(qc.posts) {
		//log.Print("proccessing query: ", qstr)
		for ;qc.nextPost < db.nextid; qc.nextPost++ {
			post := db.GetPost(qc.nextPost)
			if post != nil && q.CheckQuery(post) {
				qc.posts = append(qc.posts, post.ID)
			}
		}
		//log.Print("query processed: ", qstr)
	}
	db.Unlock()
	if version < 0 || version > len(qc.posts) {
		version = len(qc.posts)
	}
	postList := qc.posts[:version]
	// cap length of slice
	end_pos := offset + count
	if end_pos > len(postList) {
		end_pos = len(postList)
	}
	if end_pos > offset {
		if sm == SM_NEWEST {
			r := make([]PostID, 0, end_pos-offset)
			for i := offset; i < end_pos; i+=1 {
				// effectivly reverse order of postList by indexing from end
				r = append(r, postList[len(postList)-1-i])
			}
			return r, version, len(postList)
		}
		return postList[offset:end_pos], version, len(postList)
	}
	return []PostID{}, version, len(postList)
}

type QCache struct {
	// id of the last checked post + 1
	nextPost PostID
	// list of posts that matched the query.
	// in order from oldest to newest.
	posts []PostID
}
