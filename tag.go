package fediv

import "sort"
import "log"

// TagSet compactly represents what users added what tags
// fields are only exported so they can be manipulated by encoding/json,
// this should be treated like an abstract data type and only interated with via
// methods.
type TagSet struct {
	Tags        []string   `json:"tags"`
	TagsGivenBy [][]uint32 `json:"tagsGivenBy"`
	Users       []string   `json:"users"`
}

// HasTag checks if the given user added a tag.
// if user is the empty string, checks if any user added a tag.
func (ts *TagSet) HasTag(tag, user string) bool {
	for i := range ts.Tags {
		if ts.Tags[i] == tag {
			if user == "" {
				return len(ts.TagsGivenBy[i]) != 0
			}
			for k := range ts.TagsGivenBy[i] {
				uIdx := ts.TagsGivenBy[i][k]
				if user == ts.Users[uIdx] {
					return true
				}
			}
		}
	}
	return false
}

func (ts *TagSet) AddTag(tag, user string) {
	// TODO: validate usernames (they can contain up to 1 @)
	if !validName(tag) {
		// silently ignore invalid tags
		return
	}
	log.Print("adding tag: ", tag)
	// TODO: case fold tag name
	tagIdx := findOrAppendStr(&ts.Tags, tag)
	userIdx := findOrAppendStr(&ts.Users, user)
	if tagIdx < len(ts.TagsGivenBy) {
		a := ts.TagsGivenBy[tagIdx]
		a = append(a, uint32(userIdx))
	} else {
		ts.TagsGivenBy = append(ts.TagsGivenBy, []uint32{uint32(userIdx)})
	}
	return
}

// DelAllFrom removes all instances of the given user adding tags
func (ts *TagSet) DelAllFrom(user string) {
	userIdx := findStr(ts.Users, user)
	// given user has not added any tags
	if userIdx == -1 { return }
	// for each tag, iterate through the list of users to add that tag.
	for i, tagSrc := range ts.TagsGivenBy {
		// remove the specified userIdx from the list
		ntagSrc := make([]uint32, 0, len(tagSrc))
		for _, otherUserIdx := range tagSrc {
			if int(otherUserIdx) != userIdx {
				ntagSrc = append(ntagSrc, otherUserIdx)
			}
		}
		ts.TagsGivenBy[i] = ntagSrc
	}
}

type TagCountSlice []struct{ Name string; Count int }

func (tc TagCountSlice) SortByCount() {
	sort.Slice(tc, func(i, j int) bool { return tc[i].Count > tc[j].Count })
}

func (ts *TagSet) TagCountsRaw() TagCountSlice {
	r := make(TagCountSlice, len(ts.Tags))
	k := 0
	for i := range ts.Tags {
		l := len(ts.TagsGivenBy[i])
		// ignore tags with a count of zero.
		// these are tags that were added and then removed.
		if l != 0 {
			r[k].Name = ts.Tags[i]
			r[k].Count = l
			k += 1
		}
	}
	return r[:k]
}

func (ts *TagSet) TagCountsSC() TagCountSlice {
	r := ts.TagCountsRaw()
	r.SortByCount()
	return r
}
