package fediv

import (
	"html/template"
	"net/url"
	"fmt"
)

// argument struct to header template
type HeaderArgs struct {
	Username string
	// value that will appear in the search bar
	ActiveSearch string
}

type PostArgs struct {
	Post
	Username string
}

// argument struct to addtags template
type TagArgs struct {
	Post
	Username string
}

type TagListItem struct{
	Name string
	Count int
	Checked bool
}

func (ta *TagArgs) TagList() []TagListItem {
	tc := ta.Post.Tags.TagCountsSC()
	r := make([]TagListItem, len(tc))
	for i := range tc {
		r[i].Name = tc[i].Name
		r[i].Count = tc[i].Count
		r[i].Checked = ta.Tags.HasTag(tc[i].Name, ta.Username)
	}
	return r
}

type ComposeArgs struct {
	V url.Values
}

func (ca *ComposeArgs) Get(v string) string {
	list := ca.V[v]
	if len(list) > 0 {
		return list[0]
	}
	return ""
}

// produce output for template.  attributes for <input> element
func (ca *ComposeArgs) I(name string) template.HTMLAttr {
	return template.HTMLAttr(
		fmt.Sprintf("name='%s' value='%s'",
			template.HTMLEscapeString(name),
			template.HTMLEscapeString(ca.Get(name))))
}

type UserArgs struct {
	User
	AdminView bool
}

// TODO: <title> tags
var templ = template.Must(template.New("main").Parse(`
{{define "header"}}
<link rel="stylesheet" href="/static/style.css"/>
<header>
{{if eq "anon" .Username}}
<a href="/login">login</a>
<a href="/signup">signup</a>
{{else}}
<a href="/user/{{.Username}}">@{{.Username}}</a>
<a href="/profile">edit profile</a>
<a href="/logout">logout</a>
{{end}}
<a href="/q/">home</a>
<a href="/compose">new post</a>
<form class="inline" action="/search" method="GET">
<input name="q" value="{{.ActiveSearch}}"/>
<input value="search" type="submit"/>
</form>
<hr/>
</header>
{{end}}

{{/* TODO: should CWs be processed here instead of in timelines?
TODO: make timestamp nice (and adjust to current timezone)
TODO: add og: meta tags for link previews
TODO: delete button only shows if the logged in user can delete the post
*/}}
{{define "post"}}
<a href='{{.UserPage}}'>@{{.By}}</a> on {{.Created}}<br/>
{{if .CW}}<details><summary>{{.CW}}</summary><br/>{{end}}
{{.Body}}
{{range .Media}}
 <div>
 {{if eq .Kind 3}}
  <img src='{{.URL}}' alt='{{.Alt}}' title='{{.Alt}}'/>
 {{else if eq .Kind 4}}
  <a href='{{.URL}}'>{{.LinkName}}</a></br>
  {{if .Preview}}
   <blockquote>
   {{if .Preview.Image}}<img src='{{.Preview.Image}}'/>{{end}}
   {{if .Preview.Body}}<div>{{.Preview.Body}}</div>{{end}}
   </blockquote>
  {{end}}
 {{else}}
  <a href='{{.URL}}'>{{.LinkName}}</a>
 {{end}}
 </div>
{{end}}
{{range .Tags.TagCountsSC}}<a href='/q/%23{{.Name}}'>#{{.Name}}</a> {{end}}
{{if .CW}}</details>{{end}}
<br/>
<a href='/post/{{.ID}}'>view</a>
<a href='/save/{{.ID}}'>save</a>
<a href='/delete/{{.ID}}'>delete</a>
{{if .Source}}<a href='{{.Source}}'>original</a>{{end}}
{{end}}

{{define "paginate"}}
  <a href='{{.First}}'>first</a>
  <a href='{{.Prev}}#'>prev</a>
  <a href='{{.Next}}#'>next</a>
  <a href='{{.Last}}#'>last</a>
  <br/>
{{end}}

{{define "timeline"}}
 {{template "paginate" .}}
 <hr/>
 {{range .GetPosts}}
  {{template "post" .}}
  <hr/>
 {{end}}
 {{template "paginate" .}}
{{end}}

{{define "addtags"}}
  <p>add tags to post</p>
<form action='/addtags/{{.ID}}' method='POST'>
  <input name='tag' type='text' placeholder='tag1 tag2 etc'/><br/>
  {{range .TagList}}
    <label>#{{.Name}} <input type='checkbox' name='tag' value='{{.Name}}' {{if .Checked}}checked{{end}}/></label> {{.Count}}<br/>
  {{end}}
  <input type='submit' value='save'/>
</form>
{{end}}

{{define "editprofile"}}
  <h1>{{.Username}}</h1>
  <form action='/editprofile' method='POST'>
    <textarea name='bio'>{{.Bio}}</textarea><br/>
    <label>sensitive</label><br/>
    <textarea name='sensitive'>{{.Sensitive.Raw}}</textarea><br/>
    <label>hidden</label><br/>
    <textarea name='hidden'>{{.Hidden.Raw}}</textarea><br/>
    <label>following</br>
      <textarea name='following'>{{.Following.Raw}}</textarea>
    </label>
    </br>
    <input type='submit' value='update profile'/>
  </form>
{{end}}

{{define "userpage"}}
<h1>{{.Username}}</h1>
<pre>{{.Bio}}</pre>
<br/>
<a href='/q/@{{.Username}}'>view posts</a>
{{if .AdminView}}
  <h2>admin options</h2>
  <form action='/setperm/{{.Username}}' method='POST'>
  {{range .Perm.List}}
    <label>{{.Name}}<input name='perm' value='{{.ID}}' type='checkbox' {{if .Has}}checked{{end}}/></label><br/>
  {{end}}
  <input type='submit' value='update permissions'/>
  </form>
{{end}}
{{end}}

{{define "compose"}}
<form method='POST' action='/submit'>
<select name='fmt'>
  <option value='plain' selected>plain</option>
  <option value='html'>html</option>
</select><br/>
<textarea name='body'>{{.Get "body"}}</textarea><br/>
{{- /* TODO: make it possible to attach multiple media items
     * also maybe use figset here and put these on the same line */ -}}
media url: <input {{.I "url"}}/><br/>
media description: <input {{.I "alt"}}/><br/>
post tags (space seperated): <input {{.I "tags"}}/><br/>
<input type='submit' value='create post'/>
</form>
{{end}}

{{define "plaintext"}}<pre>{{.}}</pre>{{end}}

{{define "delete"}}
<p>are you sure you want to delete this post? this cannot be undone.</p>
<form method='POST' action='/delete/{{.ID}}'>
<input type='submit' value='permenantly delete post'/>
<hr/>
{{template "post" .}}
{{end}}
`))
