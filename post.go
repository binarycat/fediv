package fediv

import (
	"html/template"
	"time"
	"net/http"
	"log"
	"strings"
	"fmt"

	"github.com/microcosm-cc/bluemonday"
)

// MediaKind groups media types based on how they are handled.
// used in persistant data, so the order should not be changed.
type MediaKind uint8

const (
	// have not yet tried to get media kind
	MK_NULL MediaKind = iota
	// error getting media kind
	MK_ERROR
	// unrecognized media type, unable to map onto media kind
	MK_OTHER
	MK_IMAGE
	MK_HTML
)

// uniquly identifies a post.
type PostID uint64

type Post struct {
	ID PostID
	// account name of creator
	By string
	// activitypub id of author
	AuthorURI string
	// remote ActivityPub id of post
	Source string
	// content warning
	CW string
	// raw unsanitized html body
	RawBody string
	// attached images
	Media []Media
	// creation timestamp
	Created time.Time
	Tags TagSet
	// TODO: store how they were made, local, activitypub, rss, etc
	// TODO: SourceCreated field, stores the creation time specified in the remote ActivityPub object
	// TODO: SourceType field, specifies the type of the feed the post is from
}

// creates a new Post from an ActivityPub Note object
// may panic if given an invalid note (TODO: defer/recover)
func NewPostFromNote(note map[string]any) (post *Post, err error) {
	authorURI := note["attributedTo"].(string)
	username, err := getRemoteUser(authorURI)
	if err != nil { return nil, err }
	// TODO: set Created appropriatly? (if this is done, sorting by newest and sorting by id will be different)
	post = &Post{
		AuthorURI: authorURI,
		By: username,
		RawBody: note["content"].(string),
		Source: note["id"].(string),
		CW: idxStr(note, "summary"),
	}
	if note["inReplyTo"] != nil {
		post.Media = append(post.Media, Media{ URL: idxStr(note, "inReplyTo") })
	}
	for _, attRaw := range note["attachment"].([]any) {
		att := attRaw.(map[string]any)
		post.Media = append(post.Media,
			Media{
				URL: att["url"].(string),
				Alt: idxStr(att, "name"),
			})
	}
	for _, tagRaw := range note["tag"].([]any) {
		//log.Print("proccessing tag: ", tagRaw)
		tag := tagRaw.(map[string]any)
		if tag["type"].(string) == "Hashtag" {
			post.Tags.AddTag(tag["name"].(string)[1:], username)
		} else {
			//log.Print("tag is not hashtag: ", tag)
		}
	}
	// TODO: if "sensitive" attribute is true, add #sensitive
	return post, nil
}

func (post *Post) Clone() *Post {
	newPost := *post
	return &newPost
}

func (post *Post) Poke() {
	for i := range post.Media {
		post.Media[i].Poke()
		if post.Media[i].Kind == MK_NULL {
			log.Print("poke did not change media kind: ", post.Media[i].URL)
		}
	}
}

func (post *Post) Body() template.HTML {
	s := bluemonday.UGCPolicy().Sanitize(post.RawBody)
	return template.HTML(s)
}

func (post *Post) UserPage() string {
	if post.AuthorURI != "" {
		return post.AuthorURI
	}
	return fmt.Sprintf("/user/%s", post.By)
}

type Media struct {
	// image url
	URL string
	// image caption
	Alt string
	Kind MediaKind
	Preview *Preview
}

// Poke performs a head request on the given media.
func (m *Media) Poke() {
	// don't poke a url multiple times
	if m.Kind != MK_NULL { return }
	resp, err := http.Head(m.URL)
	if err != nil {
		m.Kind = MK_ERROR
		log.Printf("unable to Poke %s: %s", m.URL, err)
		return
	}
	log.Print("poking media: ", m)
	// TODO: webpages should show title
	if strings.HasPrefix(resp.Header.Get("Content-Type"), "image") {
		m.Kind = MK_IMAGE
		// TODO: also handle xhtml
	} else if strings.HasPrefix(resp.Header.Get("Content-Type"), "text/html") {
		m.Kind = MK_HTML
		m.Preview, err = NewPreview(m.URL)
		if err != nil {
			log.Printf("error getting preview of %s: %s", m.URL, err)
		}
		log.Print("got preview: ", m.Preview)
	} else {
		m.Kind = MK_OTHER
	}
}

func (m *Media) LinkName() string {
	if m.Alt != "" { return m.Alt }
	if m.Preview != nil && m.Preview.Title != "" { return m.Preview.Title }
	return m.URL
}

