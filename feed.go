package fediv

import (
	"io"
	"net/http"
	"log"
	"encoding/json"
	"strings"
	"time"
)

const MT_JSONLD = "application/ld+json"
const MT_AP = "application/activity+json"
type Feed struct {
	URL string
	Type string
	LastChecked time.Time
}

// Get retrives the body of the given feed.
// it sets the Type field if it is not otherwise set.
func (feed *Feed) Get() (io.ReadCloser, error) {
	r, err := http.NewRequest("GET", feed.URL, nil)
	if err != nil { return nil, err }
	if feed.Type == "" {
		r.Header.Set("Accept", MT_JSONLD)
	} else {
		r.Header.Set("Accept", feed.Type)
	}
	resp, err := http.DefaultClient.Do(r)
	if err != nil { return nil, err }
	if feed.Type == "" {
		mimeParts := strings.SplitN(resp.Header.Get("Content-Type"), ";", 2)
		feed.Type = mimeParts[0]
	}
	log.Print("retrived feed: ", feed.Type, " ", feed.URL)
	return resp.Body, nil
}

// retrive up to maxCount posts from the given feed
func (feed *Feed) Pull(maxCount int) ([]*Post, error) {
	posts := make([]*Post, 0, maxCount)
	rdr, err := feed.Get()
	if err != nil { return posts, err }
	defer rdr.Close()
	// TODO: more advanced media type handling
	if feed.Type == MT_JSONLD || feed.Type == MT_AP {
		data := make(map[string]any)
		decoder := json.NewDecoder(rdr)
		err = decoder.Decode(&data)
		if err != nil { return posts, err }
		err = pullOutbox(data, maxCount, feed.LastChecked, &posts)
		if err != nil { return nil, err }
	} else {
		panic("TODO")
	}
	feed.LastChecked = time.Now()
	
	return posts, nil
}

// pull from an ActivityPub OrderedCollection object
// TODO: stop getting older posts once duplicate posts are found?
func pullOutbox(data map[string]any, maxCount int, minTime time.Time, posts *[]*Post) error {
	if len(*posts) >= maxCount { return nil }
	if data["type"] == "OrderedCollection" {
		pageData, err := getObject(data["first"].(string))
		if err != nil { return err }
		return pullOutbox(pageData, maxCount, minTime, posts)
	} else if data["type"] == "OrderedCollectionPage" {
		for _, rawActivity := range data["orderedItems"].([]any) {
			act := rawActivity.(map[string]any)
			published, err := time.Parse(time.RFC3339, idxStr(act, "published"))
			if err != nil { return err }
			// encountered post created before the last check,
			// posts from here will have been processed by a previous pull,
			// return early.
			if published.Before(minTime) { return nil }
			if act["type"] == "Create" {
				note := act["object"].(map[string]any)
				post, err := NewPostFromNote(note)
				if err != nil { return err }
				*posts = append(*posts, post)
			}
			// TODO: handle "announce" objects (from boosts)? (maybe make that an option in the feed somehow??)
		}
		// keep going to next pages if we have less than maxCount posts
		if len(*posts) < maxCount {
			nextPage, err := getObject(data["next"].(string))
			if err != nil { return nil }
			return pullOutbox(nextPage, maxCount, minTime, posts)
		}
		return nil
	} else if data["type"] == "Person" || data["Type"] == "Service" {
		outbox, err := getObject(data["outbox"].(string))
		if err != nil { return err }
		return pullOutbox(outbox, maxCount, minTime, posts)
	} else {
		return BadObject
	}
}

func StartFeedWorker(db *DB, cfg FeedConfig) chan<- string {
	ch := make(chan string, cfg.Queue)
	m := make(map[string]*Feed)
	log.Print("starting feed worker")
	go func() {
		for u := range ch {
			feed, ok := m[u]
			if !ok {
				feed = &Feed{ URL: u }
				m[u] = feed
			}
			// feed has been retrived recently, don't retrive again.
			if time.Now().Sub(feed.LastChecked) < cfg.RecheckFeedCap {
				log.Printf("feed %s has been checked recently, skipping",
					feed.URL)
				continue
			}
			// TODO: number of posts to pull should be a server config option?
			posts, err := feed.Pull(64)
			if err != nil {
				log.Printf("error pulling feed %s: %s", u, err)
				continue
			}
			log.Printf("got %d posts from %s", len(posts), u)
			
			for i := range posts {
				// iterate over the array backwards to add older posts first.
				post := posts[len(posts)-1-i]
				existingID := db.GetPostBySource(post.Source)
				if existingID != nil { continue }
				log.Printf("feed %s has new post: %s", u, post.Source)
				err := db.AddPost(post)
				if err != nil {
					log.Printf("error adding post %s from feed: %s",
						post.Source, u)
				}
			}
		}
	}()
	// periodically re-check feeds
	go func() {
		for {
			time.Sleep(cfg.RecheckBatchSleep)
			for _, feed := range m {
				log.Print("queuing feed for updating: ", feed.URL)
				ch <- feed.URL
				time.Sleep(cfg.RecheckFeedSleep)
			}
		}
	}()
	return ch
}
