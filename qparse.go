
package fediv

import (
	"log"
)

// parse a query.
// designed to be lenient, as these queries will be written by users.
func ParseQuery(query, user string) (Query, error) {
	var pos int
	return parseQuery(query, user, &pos)
}

func parseQuery(query, user string, pos *int) (Query, error) {
	var r OrAndQuery
	var start int = -1
	var invert bool
	r = append(r, []Query{})
	addq := func(q Query) {
		if invert { q = NotQuery{ Q: q } }
		//log.Print("added: ", q)
		r[len(r)-1] = append(r[len(r)-1], q)
		invert = false
	}
	ident := func() {
		if start != -1 && start != *pos {
			str := query[start:*pos]
			q, err := ParseSimpleQuery(str, user)
			if err != nil {
				log.Printf("error parsing query %s: %s", str, err)
			} else {
				addq(q)
			}
		}
		start = -1
	}
	//log.Print("begin parsing: ", start, *pos, ": ", query)
	for ;*pos < len(query); *pos += 1 {
		//log.Print("parsing: ", start, *pos)
		switch query[*pos] {
		case ',', '|':
			//log.Print("paresed OR")
			ident()
			r = append(r, []Query{})
		case ' ', '&':
			ident()
		case '-', '!':
			invert = !invert
		case ')':
			goto End
		case '(':
			ident()
			*pos += 1
			q, err := parseQuery(query, user, pos)
			if err != nil {
				log.Printf("error parsing query: %s", err)
			} else {
				addq(q)
			}
		default:
			if start == -1 { start = *pos }
		}
	}
End:
	ident()
	//log.Printf("parsed: %#v", r)
	// unwrap single queries
	if len(r) == 1 && len(r[0]) == 1 {
		return r[0][0], nil
	}
	return r, nil
}
