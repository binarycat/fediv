package fediv

import (
	"golang.org/x/crypto/bcrypt"
)

// TODO: permission field(s) for if a user is allowed to post or add tags.
// TODO: a bunch of the mastodon features like private notes
// TODO: allow tags to be added to users?
// TODO: "created" timestamp?
type User struct {
	Username string
	PassHash []byte
	Bio string
	Sensitive QueryList
	Hidden QueryList
	Perm Permission
	Following URLList
}

func NewUser(name, pass string) *User {
	// bcrypt automatically includes salt
	phash, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.MinCost)
	if err != nil { panic(err) }
	return &User{
		Username: name,
		PassHash: phash,
	}
}

func (user *User) Login(pass string) bool {
	return bcrypt.CompareHashAndPassword(user.PassHash, []byte(pass)) == nil
}

func (user *User) CanDelete(post *Post) bool {
	if user.Username == "anon" { return false }
	if user.Perm & PermAdmin != 0 { return true }
	if user.Username == post.By { return true }
	return false
}

//func (user *User) FollowingText() string {
//	return strings.Join(user.Following, "\n")
//}

// user permission bitflags
type Permission uint16

const (
	// admin account, can delete any post, also can give permissions to other users
	PermAdmin Permission = 1 << iota
	// ability to create new posts
	PermPost
	// ability to add tags to own and other posts
	// TODO: this is still unchecked
	PermTag
	// TODO: add permision for editing profile (following people, setting bio...)
	// TODO: permission for being able to query for posts.  can be used so logged out users can't see the timeline.
)

// permission description, for iterating through in templates
type PermDesc struct{
	Name string
	ID Permission
	Has bool
}

func (perm Permission) List() []PermDesc {
	return []PermDesc{
		{ "admin", PermAdmin, perm & PermAdmin != 0 },
		{ "post", PermPost, perm & PermPost != 0 },
		{ "tag", PermTag, perm & PermTag != 0 },
	}	
}
