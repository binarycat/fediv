package main

import (
	"net/http"
	"log"
	"git.envs.net/binarycat/fediv"
)

func main() {
	var err error
	srv := new(fediv.Srv)
	srv.DB, err = fediv.NewDB()
	if err != nil { panic(err) }
	server := &http.Server{
		Addr: ":8080",
		Handler:  srv,
	}
	log.Print("server starting")
	panic(server.ListenAndServe())
}

