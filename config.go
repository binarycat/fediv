package fediv

import "time"

// server config options
type Config struct {
	Feed FeedConfig
	// TODO: default permissions for new accounts
}

// feedworker config options
type FeedConfig struct {
	// time to wait between batches of feed rechecks
	RecheckBatchSleep time.Duration
	// time to wait between individual feed rechecks
	RecheckFeedSleep time.Duration
	// minimum amount of time between checking the same feed again
	RecheckFeedCap time.Duration
	// buffer size for feed channel
	Queue int
}

var DefaultCfg = Config{
	Feed: FeedConfig{
		RecheckBatchSleep: time.Second * 120,
		RecheckFeedSleep: time.Second * 10,
		RecheckFeedCap: time.Second * 5,
		Queue: 256,
	},
}
