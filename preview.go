package fediv

import (
	"log"
	
	"github.com/antchfx/htmlquery"
	"github.com/antchfx/xpath"
)

type Preview struct {
	Title, Image, Body string
}

var titleXPath = xpath.MustCompile("//title/text()")
var imageXPath = xpath.MustCompile(
	`//meta[@property="og:image"]/@content`)
var bodyXPath = xpath.MustCompile(
	`//meta[@property="og:description" or @name="description"]/@content`)

func xpathGet(expr *xpath.Expr, nav xpath.NodeNavigator) string {
	iter := expr.Select(nav.Copy())
	for iter.MoveNext() {
		log.Printf("node: %#v", iter.Current())
		val := iter.Current().Value()
		if val != "" {
			log.Printf("found match for xpath %q: %s", expr, val)
			return val
		}
	}
	return ""
}

func NewPreview(u string) (*Preview, error) {
	node, err := htmlquery.LoadURL(u)
	if err != nil { return nil, err }
	return &Preview{
		Image: xpathGet(imageXPath, htmlquery.CreateXPathNavigator(node)),
		Title: xpathGet(titleXPath, htmlquery.CreateXPathNavigator(node)),
		Body: xpathGet(bodyXPath, htmlquery.CreateXPathNavigator(node)),
	}, nil
}



