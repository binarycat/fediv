package fediv

import (
	"log"
	"net/http"
	"strings"
	"strconv"
	"fmt"
	"encoding/base64"
	"crypto/rand"
	"errors"
	"net/url"
)

// TODO: server config field, allow disabling things like anon posting
// TODO: server config option: default permissions for new users
type Srv struct {
	DB *DB
	Session map[string]string
	FeedChan chan<- string
	Cfg Config
}

func (srv *Srv) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Content-Type", "text/html; charset=utf-8")
	p := req.URL.Path
	username := "anon"
	// value that will appear in the searchbar
	activeSearch := ""
	sessionCookie, _ := req.Cookie("session")
	if sessionCookie != nil {
		name, ok := srv.Session[sessionCookie.Value]
		if ok {
			username = name
		} else {
			log.Print("bad session cookie: ", sessionCookie)
		}
	}
	redir := func(loc string) {
		resp.Header().Set("Location", loc)
		resp.WriteHeader(303)
	}
	found := func(tem string, arg any) {
		err := templ.ExecuteTemplate(resp, "header",
			&HeaderArgs{ username, activeSearch })
		if err != nil { log.Print("template error: ", err) }
		err = templ.ExecuteTemplate(resp, tem, arg)
		if err != nil { log.Print("template error: ", err) }
	}
	// TODO: /static/* for stuff like css styles
	if p == "" || p == "/" || p == "/q" {
		redir("/q/")
	} else if strings.HasPrefix(p, "/static/") {
		// TODO: cache control
		if !getStatic(p[8:], resp) { goto NotFound }
	} else if strings.HasPrefix(p, "/post/") {
		n, err := strconv.Atoi(p[6:])
		if err != nil {
			log.Printf("request error: %s", err)
			goto NotFound
		}
		post := srv.DB.GetPost(PostID(n))
		if post == nil { goto NotFound }
		found("post", post)
	} else if strings.HasPrefix(p, "/q/") {
		q, err := ParseQuery(p[3:], username)
		if err != nil {
			log.Printf("query error: %s", err)
			// TODO: this should prolly return a different status, maybe bad request?
			goto NotFound
		}
		param := req.URL.Query()
		version, err := strconv.Atoi(param.Get("v"))
		if err != nil { version = -1 }
		pageNo, err := strconv.Atoi(param.Get("page"))
		if err != nil { pageNo = 0 }
		if pageNo < 0 { pageNo = 0 }
		count, err := strconv.Atoi(param.Get("count"))
		// TODO: make this default value configurable
		if err != nil { count = 10 }
		posts, nversion, nposts :=
			srv.DB.GetPage(q, SM_NEWEST, version, pageNo*count, count)
		if nversion != version {
			param.Set("v", strconv.Itoa(nversion))
			param.Set("page", strconv.Itoa(pageNo))
			param.Set("count", strconv.Itoa(count))
			req.URL.RawQuery = param.Encode()
			resp.Header().Set("Location", req.URL.String())
			resp.WriteHeader(303)
			log.Printf("redirecting to %s", req.URL)
			return
		}
		activeSearch = q.String()
		// TODO: option to return rss instead?
		found("timeline", &Timeline{
			DB: srv.DB,
			Posts: posts,
			URL: req.URL,
			Pages: (nposts-1)/count+1,
			User: srv.DB.GetUser(username),
		})
	} else if p == "/compose" {
		found("compose", &ComposeArgs{V: req.URL.Query()})
	} else if p == "/submit" {
		// TODO: more explanatory error message saying you do not have the correct permissions to post
		if !srv.HasPerm(username, PermPost) { goto Forbidden }
		post := &Post{
			By: username,
			Source: req.FormValue("source"),
			RawBody: req.FormValue("body"),
		}
		if req.FormValue("fmt") == "plain" {
			var bldr strings.Builder
			err := templ.ExecuteTemplate(&bldr, "plaintext", post.RawBody)
			if err != nil { log.Print("template error: ", err) }
			post.RawBody = bldr.String()
		}
		mediaURL := req.FormValue("url")
		if mediaURL != "" {
			post.Media = []Media{{ URL: mediaURL, Alt: req.FormValue("alt") }}
		}
		tags := strings.Split(req.FormValue("tags"), " ")
		for _, tag := range tags {
			post.Tags.AddTag(tag, post.By)
		}
		post.Poke()
		err := srv.DB.AddPost(post)
		if err != nil { log.Printf("failed to make post: %s", err) }
		redir(fmt.Sprintf("/post/%d", post.ID))
	} else if p == "/signup" {
		fmt.Fprintf(resp, "%s", signupForm)
	} else if p == "/newuser" {
		name := req.FormValue("name")
		pass1 := req.FormValue("pass1")
		pass2 := req.FormValue("pass2")
		if pass1 != pass2 {
			resp.WriteHeader(400)
			fmt.Fprint(resp, "password mismatch")
		}
		if len([]byte(pass1)) > 60 {
			resp.WriteHeader(400)
			fmt.Fprint(resp, "password is too long")
		}
		// TODO: check if username is already registered!!
		err := srv.DB.PutUser(NewUser(name, pass1))
		if err != nil {
			log.Print("failed to create new user: ", err)
			resp.WriteHeader(500)
			fmt.Fprint(resp, "internal server error")
		}
		redir("/login")
	} else if p == "/login" {
		// TODO: redirect paramater (or maybe user referer header) so that after login you return to the page you hit the login button on
		fmt.Fprintf(resp, "%s", loginForm)
	} else if p == "/newsession" {
		name := req.FormValue("name")
		pass := req.FormValue("pass")
		if name == "anon" {
			goto LoginFail
		}
		user := srv.DB.GetUser(name)
		if user == nil {
			goto LoginFail
		}
		if !user.Login(pass) {
			goto LoginFail
		}
		http.SetCookie(resp, srv.NewSessionCookie(user))
		srv.PullFeeds(user.Following.Items())
		redir(fmt.Sprintf("/user/%s", name))
	} else if p == "/logout" {
		delete(srv.Session, sessionCookie.Value)
		redir("/")
	} else if strings.HasPrefix(p, "/save/") {
		postID, err := strconv.Atoi(p[6:])
		if err != nil { goto NotFound }
		post := srv.DB.GetPost(PostID(postID))
		if post == nil { goto NotFound }
		// TODO: default value of checkboxes depend on if the current user has already added that tag to the given post.  this means this is also used for removing tags.
		found("addtags", &TagArgs{Post: *post, Username: username})
	} else if strings.HasPrefix(p, "/addtags/") {
		postID, err := strconv.Atoi(p[9:])
		if err != nil { goto NotFound }
		post := srv.DB.GetPost(PostID(postID))
		if err != nil { goto NotFound }
		req.ParseForm()
		post.Tags.DelAllFrom(username)
		for _, tag := range req.Form["tag"] {
			// TODO: split tag on spaces
			post.Tags.AddTag(tag, username)
		}
		err = srv.DB.PutPost(post)
		if err != nil { log.Print("error saving post: ", err) }
		redir(fmt.Sprintf("/post/%d", post.ID))
	} else if p == "/profile" {
		user := srv.DB.GetUser(username)
		if user == nil {
			log.Print("logged in as nonexistant user: ", username)
			goto NotFound
		}
		found("editprofile", user)
	} else if p == "/editprofile" {
		user := srv.DB.GetUser(username)
		if user == nil {
			log.Print("logged in as nonexistant user: ", username)
			goto NotFound
		}
		user.Bio = req.FormValue("bio")
		user.Sensitive.SetRaw(req.FormValue("sensitive"))
		user.Hidden.SetRaw(req.FormValue("hidden"))
		user.Following.SetRaw(req.FormValue("following"))
		srv.DB.PutUser(user)
		redir("/profile")
		srv.PullFeeds(user.Following.Items())
	} else if strings.HasPrefix(p, "/user/") {
		user := srv.DB.GetUser(p[6:])
		if user == nil { goto NotFound }
		found("userpage", &UserArgs{
			User: *user,
			AdminView: srv.IsAdmin(username),
		})
	} else if p == "/search" {
		// either calls Pull or dispatches to /q/
		param := req.URL.Query()
		q := param.Get("q")
		u, err := url.Parse(q)
		if err == nil && u.IsAbs() {
			postID, err := srv.Pull(q)
			if err != nil {
				log.Print("/search: pull error: ", err)
				goto NotFound
			}
			redir(fmt.Sprintf("/post/%d", postID))
		} else {
			redir(fmt.Sprintf("/q/%s", url.PathEscape(q)))
		}
	} else if strings.HasPrefix(p, "/delete/") {
		post := srv.GetPost(p[8:])
		if post == nil { goto NotFound }
		if req.Method == "POST" {
			user := srv.DB.GetUser(username)
			if user != nil && user.CanDelete(post) {
				err := srv.DB.DelPost(post.ID)
				if err != nil {
					log.Print("error deleting post ", post.ID, ": ", err)
				} else {
					fmt.Fprint(resp, "post deleted")
				}
			} else {
				goto Forbidden
			}
		} else {
			found("delete", post)
		}
	} else if strings.HasPrefix(p, "/setperm/") {
		user := srv.DB.GetUser(p[9:])
		if user == nil { goto NotFound }
		if !srv.IsAdmin(username) { goto Forbidden }
		var newPerm Permission
		err := req.ParseForm()
		if err != nil { goto Bad }
		for _, v := range req.PostForm["perm"] {
			p, err := strconv.Atoi(v)
			if err != nil { goto Bad }
			newPerm |= Permission(p)
		}
		user.Perm = newPerm
		srv.DB.PutUser(user)
		redir(fmt.Sprintf("/user/%s", user.Username))
	// TODO: /admin endpoint used to adjust server settings (such as the default permissions for new users)
	} else {
		goto NotFound
	}
	return
NotFound:
	resp.WriteHeader(404)
	fmt.Fprint(resp, "Not Found")
	return
LoginFail:
	resp.WriteHeader(401)
	fmt.Fprint(resp, "Login Failed")
	return
Forbidden:
	resp.WriteHeader(403)
	fmt.Fprint(resp, "Insufficient permissions")
	return
Bad:
	resp.WriteHeader(400)
	fmt.Fprint(resp, "Bad Request")
	return
}

var signupForm = `
<form method='POST' action='/newuser'>
username: <input name='name'/><br/>
password: <input name='pass1' type='password'/><br/>
confirm password: <input name='pass2' type='password'/><br/>
<input type='submit' value='create account'/>
</form>
`

var loginForm = `
<form method='POST' action='/newsession'>
username: <input name='name'/><br/>
password: <input name='pass' type='password'/><br/>
<input type='submit' value='login'/>
</form>
`

func (srv *Srv) HasPerm(username string, perm Permission) bool {
	user := srv.DB.GetUser(username)
	return user != nil &&
		user.Perm & perm != 0
}

func (srv *Srv) IsAdmin(username string) bool {
	return srv.HasPerm(username, PermAdmin)
}

func (srv *Srv) GetPost(postid string) *Post {
	id, err := strconv.Atoi(postid)
	if err != nil { return nil }
	return srv.DB.GetPost(PostID(id))
}

func (srv *Srv) NewSessionCookie(user *User) *http.Cookie {
	var buf [256]byte
	_, err := rand.Read(buf[:])
	if err != nil {
		log.Fatal("failed to generate session token: ", err)
	}
	token := base64.RawURLEncoding.EncodeToString(buf[:])
	_, conflict := srv.Session[token]
	if conflict {
		// should statistically never happen
		log.Fatal("session token already exists: ", token)
	}
	if srv.Session == nil {
		srv.Session = make(map[string]string)
	}
	srv.Session[token] = user.Username
	return &http.Cookie{
		Name: "session",
		Value: token,
		SameSite: http.SameSiteStrictMode,
		Secure: true,
	}
}

var BadObject = errors.New("bad ActivityPub object")

// Srv.Pull makes a remote post available on the local instance
// may panic when retriving malformed objects (this isn't a problem as long as
// it only happens to temprory goroutines like the http request handlers)
func (srv *Srv) Pull(u string) (PostID, error) {
	note, err := getObject(u)
	if err != nil { return 0, err }
	// check if post is already in the database
	existingID := srv.DB.GetPostBySource(note["id"].(string))
	if existingID != nil { return *existingID, nil } 
	post, err := NewPostFromNote(note)
	if err != nil { return 0, err }
	//username, err := srv.PullUser(authorURI)
	//if err != nil { return 0, err }
	//post.By = username
	err = srv.DB.AddPost(post)
	if err != nil { return 0, err }
	return post.ID, nil
}

// depricated alias
func (srv *Srv) PullUser(u string) (string, error) {
	return getRemoteUser(u)
}

// cache for the result of getRemoteUser
var remoteUserCache = make(map[string]string)
// parses a remote user and returns their @
// should get moved to util.go
func getRemoteUser(u string) (string, error) {
	name, ok := remoteUserCache[u]
	if ok { return name, nil }
	person, err := getObject(u)
	if err != nil { return "", err }
	uri, err := url.Parse(u)
	if err != nil { return "", err }
	name = fmt.Sprintf("%s@%s", person["preferredUsername"], uri.Host)
	remoteUserCache[u] = name
	return name, nil
}

func (srv *Srv) PullFeeds(a []string) {
	if srv.FeedChan == nil {
		srv.FeedChan = StartFeedWorker(srv.DB, srv.Cfg.Feed)
	}
	for _, s := range a {
		srv.FeedChan <- s
	}
}
