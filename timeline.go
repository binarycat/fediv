package fediv

import (
	"net/url"
	"strconv"
	"fmt"
)

type Timeline struct {
	Posts []PostID
	DB *DB
	URL *url.URL
	// how many pages are available
	Pages int
	User *User
}

func (t *Timeline) GetPosts() []*Post {
	r := make([]*Post, 0, len(t.Posts))
	for _, id := range t.Posts {
		post := t.DB.GetPost(id)
		if post != nil {
			// TODO: check against the user's Hidden filter
			post.Poke()
			if t.User != nil {
				post.CW = fmt.Sprintf("%s%s", post.CW,
					t.User.Sensitive.Match(post, t.User.Username))
			}
			r = append(r, post)
		}
	}
	return r
}

func (t *Timeline) Next() string {
	param := t.URL.Query()
	pageNo, _ := strconv.Atoi(param.Get("page"))
	param.Set("page", strconv.Itoa(pageNo+1))
	newURL := *t.URL
	newURL.RawQuery = param.Encode()
	return newURL.String()
}

func (t *Timeline) Prev() string {
	param := t.URL.Query()
	pageNo, _ := strconv.Atoi(param.Get("page"))
	param.Set("page", strconv.Itoa(pageNo-1))
	newURL := *t.URL
	newURL.RawQuery = param.Encode()
	return newURL.String()
}

func (t *Timeline) First() string {
	param := t.URL.Query()
	param.Del("v")
	param.Del("page")
	newURL := *t.URL
	newURL.RawQuery = param.Encode()
	return newURL.String()
}

func (t *Timeline) Last() string {
	param := t.URL.Query()
	param.Set("page", strconv.Itoa(t.Pages-1))
	newURL := *t.URL
	newURL.RawQuery = param.Encode()
	return newURL.String()
}
