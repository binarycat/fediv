package fediv

import (
	"os"
	"encoding/json"
	"regexp"
	"net/http"
	"net/url"
	"io"
	"fmt"
	"log"
	"errors"
	"strings"
)

func findStr(a []string, s string) int {
	for i := range a {
		if a[i] == s {
			return i
		}
	}
	return -1
}

func findOrAppendStr(a *[]string, s string) int  {
	i := findStr(*a, s)
	if i == -1 {
		i = len(*a)
		*a = append(*a, s)
	}
	return i
}

func writeJSON(path string, v any) error {
	file, err := os.Create(path)
	if err != nil { return err }
	defer file.Close()
	err = json.NewEncoder(file).Encode(v)
	if err != nil { return err }
	return nil
}

func readJSON(path string, dst any) error {
	file, err := os.Open(path)
	if err != nil { return err }
	defer file.Close()
	return json.NewDecoder(file).Decode(&dst) 
}

var nameRegexp = regexp.MustCompile("^[a-zA-Z0-9_]+$")

// checks if name is a valid name for a tag or user
func validName(name string) bool {
	return nameRegexp.MatchString(name)
}



// get remote activitypub object
func getObject(u string) (map[string]any, error) {
	return getJSON(u, http.Header{ "Accept": {"application/activity+json"}})
}

func getJSON(u string, header http.Header) (map[string]any, error) {
	log.Print("retriving remote object: ", u)
    req, err := http.NewRequest("GET", u, nil)
    if err != nil {
        return nil, err
    }

	if header != nil {
		req.Header = header
	}

    resp, err := http.DefaultClient.Do(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("http error: %s", resp.Status)
	}

    rawBody, err := io.ReadAll(resp.Body)
    if err != nil {
        return nil, err
    }

    var obj map[string]interface{}
    err = json.Unmarshal(rawBody, &obj)
    if err != nil {
        return nil, err
    }

    return obj, nil
}

func idxStr(m map[string]any, idx string) string {
	r, ok := m[idx].(string)
	if ok { return r }
	return ""
}

// check if a given string is a valid absolute url
func isAbsURL(rawURL string) bool {
	u, err := url.Parse(rawURL)
	if err != nil { return false }
	return u.IsAbs()
}

func webfinger(uri, host string) (map[string]any, error) {
	return getJSON(fmt.Sprintf(
		"https://%s/.well-known/webfinger?resource=%s",
		uri, url.QueryEscape(uri)), nil)
}

var ErrLocalUser = errors.New("expected remote user")
var ErrField = errors.New("json object is missing field, or field has incorrect type")

// returns the url for the user's activitypub object
func userObject(user string) (string, error) {
	// TODO: cache/memozie this function
	parts := strings.Split(user, "@")
	if len(parts) != 2 { return "", ErrLocalUser }
	j, err := webfinger(fmt.Sprintf("acct:%s@%s", parts[0], parts[1]), parts[1])
	if err != nil { return "", err }
	links, ok := j["links"].([]any)
	if !ok { return "", ErrField }
	for _, rawLink := range links {
		link, ok := rawLink.(map[string]any)
		if !ok { goto F }
		rel := idxStr(link, "rel")
		ty := idxStr(link, "type")
		href := idxStr(link, "href")
		if rel == "self" && ty == MT_AP {
			return href, nil
		}
	}
F:
	return "", ErrField
}

