package fediv

import "net/http"

// get static webpage, return true if successful.
func getStatic(p string, w http.ResponseWriter) bool {
	if p == "style.css" {
		w.Header().Add("Content-Type", "text/css")
		w.Write([]byte(stylesheet))
	} else {
		return false
	}
	return true
}

const stylesheet = `
.inline { display: inline; }
img { max-width: 30vw; max-height: 50vh; }
`

