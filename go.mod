module git.envs.net/binarycat/fediv

go 1.19

require (
	github.com/microcosm-cc/bluemonday v1.0.23
	golang.org/x/crypto v0.7.0
)

require (
	github.com/antchfx/htmlquery v1.3.0 // indirect
	github.com/antchfx/xmlquery v1.3.15 // indirect
	github.com/antchfx/xpath v1.2.4 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/gorilla/css v1.0.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/text v0.8.0 // indirect
)
