package fediv

import (
	"fmt"
	"errors"
	"strings"
	"log"
)

var ErrBadQuery = errors.New("malformed query")

// TODO: query for posts that mention users.  allows you to filter out replies from the timeline.
// TODO: maybe queries should be able to match against users too?
// TODO: maybe queries should use actual relational database queries?
type Query interface {
	// return the string form of the query
	// this is used for caching the results of a query
	String() string
	// check if the query matches the given post
	CheckQuery(*Post) bool
	// TODO: function to optionally specify a query that matches a superset of posts (so the cache of that query can be reused)
}

func ParseSimpleQuery(query, user string) (Query, error) {
	if query == "" {
		return &AllQuery{}, nil
	} else if query == "local" {
		return SourceQuery(""), nil
	} else if strings.HasPrefix(query, "#") {
		if strings.HasPrefix(query, "##") {
			return &TagQuery{
				Name: query[2:],
				User: user,
			}, nil
		}
		parts := strings.SplitN(query[1:], "@", 2)
		if len(parts) == 2 {
			return &TagQuery{
				Name: parts[0],
				User: parts[1],
			}, nil
		}
		return &TagQuery{ Name: query[1:] }, nil
	} else if strings.HasPrefix(query, "@") {
		return &UserQuery{ User: query[1:] }, nil
	} else if strings.HasPrefix(query, "src:") {
		return SourceQuery(query[4:]), nil
	} else {
		return nil, ErrBadQuery
	}
}

type TagQuery struct {
	// name of tag
	Name string
	// only match posts that were given said tag by this user
	// "" means any user
	User string
}

func (q *TagQuery) String() string {
	if q.User != "" {
		return fmt.Sprintf("#%s@%s", q.Name, q.User)
	} else {
		return fmt.Sprintf("#%s", q.Name)
	}
}

func (q *TagQuery) CheckQuery(post *Post) bool {
	return post.Tags.HasTag(q.Name, q.User)
}

type AllQuery struct {}

func (*AllQuery) String() string {
	return ""
}

func (q *AllQuery) CheckQuery(post *Post) bool {
	return true
}

type UserQuery struct {
	User string
}

func (q *UserQuery) String() string {
	return fmt.Sprintf("@%s", q.User)
}

func (q *UserQuery) CheckQuery(post *Post) bool {
	return post.By == q.User
}

type SourceQuery string

func (q SourceQuery) String() string {
	return fmt.Sprintf("src:%s", string(q))
}

func (q SourceQuery) CheckQuery(post *Post) bool {
	return post.Source == string(q)
}

type NotQuery struct {
	Q Query
}

func (q NotQuery) String() string {
	return fmt.Sprintf("-%s", q.Q)
}

func (q NotQuery) CheckQuery(post *Post) bool {
	return !q.Q.CheckQuery(post)
}

// query that matches if any list of queryies all match the post
type OrAndQuery [][]Query

func (q OrAndQuery) String() string {
	r := ""
	for _, list := range q {
		for _, query := range list {
			r = fmt.Sprintf("%s (%s)", r, query)
		}
		r = fmt.Sprintf("%s,", r)
	}
	// remove trailing comma
	if len(r) != 0 && r[len(r)-1] == ',' {
		r = r[:len(r)-1]
	}
	return r
}

func (q OrAndQuery) CheckQuery(post *Post) bool {
	for _, list := range q {
		for _, query := range list {
			if !query.CheckQuery(post) {
				log.Print("subquery not matched")
				goto NoMatch
			}
		}
		return true
	NoMatch:
	}
	return false
}

type QueryList struct {
	Raw string
	cache []Query
}

func (ql *QueryList) SetRaw(raw string) {
	ql.cache = []Query{}
	ql.Raw = raw
}

func (ql *QueryList) Queries(username string) []Query {
	if len(ql.cache) != 0 { return ql.cache }
	parts := strings.Split(ql.Raw, "\n")
	// TODO: this should prolly have the capacity set, and len = 0
	ql.cache = make([]Query, len(parts))
	for i, part := range parts {
		part = strings.TrimSpace(part)
		if part == "" { continue }
		ql.cache[i], _ = ParseQuery(part, username)
	}
	return ql.cache
}

func (ql *QueryList) Match(post *Post, username string) string {
	r := ""
	for _, q := range ql.Queries(username) {
		if q != nil && q.CheckQuery(post) {
			log.Print("post is sensitive: ", q)
			r = fmt.Sprintf("%s %s", r, q)
		}
	}
	return r
}
