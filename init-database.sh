#!/bin/sh -e
dir="$HOME/.config/fediv"
mkdir "$dir"
echo "{}" > $dir/db.json
mkdir "$dir/post"
mkdir "$dir/user"
# enable anon posting
echo '{ "Username": "anon", "Permission": 2 }' > "$dir/user/anon"
